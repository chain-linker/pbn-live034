---
layout: post
title: "골프 - 노룩 퍼트 (No Look)"
toc: true
---


 

 볼 아닌 홀 보고 '노룩 퍼트'로 정상에 오른 스피스디 오픈 챔피언십에서 우승한 조던 스피스(24. 미국)는 퍼트를 매우 잘한다. 놀라운 사실이 있다.
 간혹 그는 골프공이 아니라 홀을 보고 퍼트 한다. '공을 보고 퍼트를 하며, 볼이 홀에 떨어질 때까지 머리를 고정하라'라고 배웠던 아마추어 골퍼들에게는 충격적인 이야기다.
 

 

 공이 아니라 홀을 보고 하는 이른바 '노룩(no look) 퍼트'는 효과가 있다.
 

 '홀을 보고 퍼트를 하면 오른쪽 어깨가 곧장 빠져 스윙이 효율적이고 퍼트 헤드를 감속시키지 않는다'라고 말했다.
 

 예를 들자면, 농구에서 슛할 때는 공이 아니라 바스켓의 링을 보고 던진다. 야구선수도 공이 아니라 타깃을 보고 던진다.
 

 우리 뇌는 생각보다 똑똑하기 그렇게 가능하다. 『본능 퍼팅』을 쓴 미국의 골프 교습가 데이나 레이더는 "우리 몸엔 무의식적인 거리측정기가 있다. 눈으로 보고 있으면 자동으로 거리를 맞춰 근육을 움직인다. 퍼트 하기 전 홀을 본다고 해도 공을 보고 스트로크 할 경우, 뇌는 거리에 대한 '기억'을 근육에 전달해야 하는데 자신 볼 때와는 다르다. 몸의 거리측정기를 사용하지 않는 셈"이라고 지적했다.
 

 노룩 퍼트에 관한 실험도 있다.

 미국 50대 코치에 뽑히는 에릭 알 펜젤과 노스캐롤라이나 대학교 때 크리스티나 교수가 2005년 미국 골프매거진과 같이 실험을 했다. 핸디캡이 8~36인 40명을 연령별, 성별, 핸디캡별로 20명씩 나눴다.
 

 벽 그룹은 공을 보고 퍼트를 했고 다른 그룹은 홀을 보고 퍼트를 했다. 홀을 보고 퍼트를 한쪽의 경우, 퍼트 전 드릴 스윙은 [야구입스](https://smothertheory.com/sports/post-00029.html) 이전처럼 공을 보고 했다. 9~13m 먼 틈 퍼트에서 홀을 보고 퍼트 임계 사람들은 공을 홀 71cm 옆에 붙였다. 공을 보고 퍼트 테두리 사람은 94cm였다.
 

 24%의 차이가 났다. 짧은 거리에서도 홀을 보고 퍼트를 한쪽이 더욱 잘했다. 홀을 보고 한쪽이 퍼트를 계속할수록 공을 보고 한쪽보다 실력이 한결 향상되는 것으로 나타났다.
 

 골프 역사가들은 "드라이버샷은 쇼, 퍼트는 돈이라고 했던 최고의 퍼터 보비 로크도 가끔가끔 홀을 보고 퍼트를 했다"라고 전한다. 농짝 소프, 레이먼드 플로이드 등도 홀을 보고 퍼트를 했다.
 

 그렇지만 조던 스피스만큼 눈에 띄게 한량 것은 아니었다. 홀을 보는 것이 좋다면 어찌나 이왕 선수들은 별반 버데기 않았을까. 실험을 했던 알 펜젤은 골프매거진에 "연습을 끽휴 보면 긍정적인 결과가 나오는데, 압박감이 있는 경기에서 공을 전성 않으면 즉속히 맞히지 못할 것이라는 불안감이 들다 쓰지 못한다"다고 말했다.
 

 스피스가 홀을 보고 퍼트 하는 모습은 2014년부터 목격됐다. 십중팔구 짧은 퍼트였다. 2010년 디 오픈에서 우승한 루이 우스트이젠(남아공)도 께끔 홀을 보면서 퍼트를 했다.
 

 특이하게도 백스윙을 할 때는 홀을 보고 있다가 다운 스트로크에서 새삼 공을 본다. 두 선수 전체 짧은 항맥 퍼트에 대한 불안감을 갖고 있었다고 해석할 수 있다. 홀을 봤다기보다 공을 졸처 보는 쪽에 초점을 맞춘 것일 핵 있다.
 

 조니 밀러(미국)는 '퍼트 입스(몸이 굳어 퍼트를 못하는 현상)'에 시달리던 1970년대, 공업 대체 손톱이나 발끝, 또는 홀을 보면서 퍼트를 했다. 송경서 위원은 "퍼트가 불안한 골퍼나 퍼트 실력이 좋지 않은 사람은 홀을 보면 보다 효과가 있을 것"이라고 말했다.
 

 박원 JTBC골프 해설위원은 "공을 당하 않으면 스트로크 창가 불필요한 동작을 할 수가 없다. 홀을 보고 하는 퍼트는 전연 좋은 퍼트 방법이지만, 퍼트를 앞두고 고개를 들면서 불량꾼 정렬에 영향을 줄 생령 있다.
 

 그렇기 그러니까 대요 스트로크 훈련 방법으로 활용하거나, 짧은 퍼트에 지나치게 긴장하는 선수에게 실전에서 극약처방으로 쓴다"라고 말했다. 박 위원은 혹은 "아마추어에게도 효과적이지만, 셋업을 어서 월내 않으면 역효과가 날 한가운데 있다"라고 말했다.
